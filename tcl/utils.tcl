namespace eval redis_utils {
}

proc redis_utils::dict_to_set { key_values } {
  if { ![ llength $key_values ] } {
    return "Copy( Empty )"
  }
  array set _dict {}
  set first 1
  foreach { k v } $key_values {
    if { $first } {
      set first 0
      set result "\[\[ Text $k=\"$v\""
    } else {
      append result ", Text $k=\"$v\""
    }
  }
  return [ append result " \]\]" ]
}
